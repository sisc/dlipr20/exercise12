# Correction
## Task 1 
###Task 1.1
2/2 P
###Task 1.2
missing
0/4 P
###Task 1.3
2/2 P

## Task 2 
missing 0/7 P

#Total : 4 / 15 P


**RWTH Aachen - Deep Learning in Physics Research SS2020**

**Exercise 12 RNNs**

Date: 19.07.2020

Students:
| student            | rwth ID |
|--------------------|---------|
| Baptiste Corneglio | 411835|
| Florian Stadtmann  | 367319 |
| Johannes Wasmer    |  090800 |


**Table of Contents**

- [Task 1: Sinus-wave (forecasting) [8 P]](#task-1-sinus-wave-forecasting-8-p)
    - [I. Run the provided script and comment on the output. [2 P]](#i-run-the-provided-script-and-comment-on-the-output-2-p)
    - [II. Vary the number and size of the LSTM layers and compare training time and stability of the performance. [4 P]](#ii-vary-the-number-and-size-of-the-lstm-layers-and-compare-training-time-and-stability-of-the-performance-4-p)
    - [III. Vary the window size and comment. What happens for a window size of 1? [2 P]](#iii-vary-the-window-size-and-comment-what-happens-for-a-window-size-of-1-2-p)
- [Task 2: Calculator (Sequence-to-sequence) [7 P]](#task-2-calculator-sequence-to-sequence-7-p)
    - [I. Train the provided network to ~100% valuation accuracy by choosing suitable hyperparameters for the RNN and comment on the result. [2 P]](#i-train-the-provided-network-to-100-valuation-accuracy-by-choosing-suitable-hyperparameters-for-the-rnn-and-comment-on-the-result-2-p)
    - [II. Explore the limitations of the network to generalize to different expressions. How could the generalization capability be improved? [5 P]](#ii-explore-the-limitations-of-the-network-to-generalize-to-different-expressions-how-could-the-generalization-capability-be-improved-5-p)
- [References](#references)





# Task 1: Sinus-wave (forecasting) [8 P] #

## I. Run the provided script and comment on the output. [2 P] ##

We denote the LSTM hidden layer architecture with the symbol `L`. `L=[16]` for example stands for a model with one LSTM layer with 16 units.

We denote the window size with symbol `ws`.

For the model as it was provided, that is default parameters `ws=20, L=[16]`, the prediction reproduces the sine signal perfectly.

![ws=20, sinus1](https://s3.amazonaws.com/comet.ml/image_505d4d89652a4adeb6ae4810cd8a4f72-QerKhrlSGbk7z1DPNto2r9uAX.)
![ws=20, sinus2](https://s3.amazonaws.com/comet.ml/image_505d4d89652a4adeb6ae4810cd8a4f72-q7VqCGW53RaBcoOQPVxZfEG7W.)
![ws=20, sinus3](https://s3.amazonaws.com/comet.ml/image_505d4d89652a4adeb6ae4810cd8a4f72-6EDUg2vVfkltwdilszOFTVyyz.)

The `model.fit` callback `ReduceLROnPlateau` monitors the training validation loss to reduce the learning rate, with some patience setting.

All three experiments in these charts were run with the default parameters.

chart training validation loss:

![task1.I train validation loss image](https://www.comet.ml/api/image/notes/download?imageId=1nQHz9btyLMr3itV6Bsgw4zKV&amp;objectId=dfbfc81c3df94c29911a7d371cea527f)

chart training learning rate:

![task1.I train learning rate image](https://www.comet.ml/api/image/notes/download?imageId=Gq0rvToF4e8tcLfhmjiCAKgR8&amp;objectId=dfbfc81c3df94c29911a7d371cea527f)


## II. Vary the number and size of the LSTM layers and compare training time and stability of the performance. [4 P] ##



## III. Vary the window size and comment. What happens for a window size of 1? [2 P] ##

Since the prediction with model parameters `window_size = ws = 20` with default `L=[16]` reproduces the sine signal perfectly in the visual / plot evaluation, we chose `L=[16]` here and decreased `ws`.

`ws=10`, experiment [34f91fbe9](https://www.comet.ml/irratzo/bonus/34f91fbe9bc14d2b970ac1500986ea98): only the phase is shortened.

![ws=10, sinus1](https://s3.amazonaws.com/comet.ml/image_34f91fbe9bc14d2b970ac1500986ea98-oLj4ZFpYctdd4VdzNcrhmTKoB.)
![ws=10, sinus2](https://s3.amazonaws.com/comet.ml/image_34f91fbe9bc14d2b970ac1500986ea98-jjVR4EI3SSmqtbhnMMHaWhVLN.)
![ws=10, sinus3](https://s3.amazonaws.com/comet.ml/image_34f91fbe9bc14d2b970ac1500986ea98-fEzMGWaGbWdbk62Mf8K9y95cv.)

`ws=05`, experiment [5c78e511d](https://www.comet.ml/irratzo/bonus/5c78e511d02f405f9ffcde58cdd11780): phase shortened, amplitude decreased, positive offset. These effects are stronger the nearer the starting window `i0` lies to a wave crest/trough. Also, the sine crests are not really symmetric anymore, but slant to the side. 

![ws=05, sinus1](https://s3.amazonaws.com/comet.ml/image_5c78e511d02f405f9ffcde58cdd11780-EIKKjOgcwsyO39IGjl0AMrcrC.)
![ws=05, sinus2](https://s3.amazonaws.com/comet.ml/image_5c78e511d02f405f9ffcde58cdd11780-RmRCNSKhciVowAe3kbVWUHV6h.)
![ws=05, sinus3](https://s3.amazonaws.com/comet.ml/image_5c78e511d02f405f9ffcde58cdd11780-dMXeqmWSTblBIh4HZwj2fBlV0.)

`ws=01`, experiment [aca305c59](https://www.comet.ml/irratzo/bonus/aca305c59819487abd01eac66ba83990): the predicted signal is just a constant. 

![ws=01, sinus1](https://s3.amazonaws.com/comet.ml/image_aca305c59819487abd01eac66ba83990-CNamFY7W3eALZaDqGoHqe5Btq.)
![ws=01, sinus2](https://s3.amazonaws.com/comet.ml/image_aca305c59819487abd01eac66ba83990-GE97KsdhMm1omhW4ceAbW5Has.)
![ws=01, sinus3](https://s3.amazonaws.com/comet.ml/image_aca305c59819487abd01eac66ba83990-FCndtG4zoq5qjo4emh4flJjLy.)

Plot line coloring: Orange = `ws=20`, purple = `ws=10`, teal `ws=05`, green = `ws=01`. 

[task1.III training validation loss](https://www.comet.ml/embedded-panel/?chartId=0BHcW2&amp;projectId=dfbfc81c3df94c29911a7d371cea527f&amp;viewId=8J4qzt8u5jHiJrWADneZcGQyo)

![task1.III training validation loss image](https://www.comet.ml/api/image/notes/download?imageId=OTq5KedAmjWAQELZkQ8jXiXiL&amp;objectId=dfbfc81c3df94c29911a7d371cea527f)

[task1.III learning rate](https://www.comet.ml/embedded-panel/?chartId=LR23Bi&amp;projectId=dfbfc81c3df94c29911a7d371cea527f&amp;viewId=8J4qzt8u5jHiJrWADneZcGQyo)

![task1.III learning rate image](https://www.comet.ml/api/image/notes/download?imageId=gTzCS3AL1bpfQbvc3yfjmFgZU&amp;objectId=dfbfc81c3df94c29911a7d371cea527f)

It is evident that the loss does not decrease for `ws=01` from the start, which explains the bad prediction above. This makes sense since each window in `X` only holds one single value of the signal and `y` its successor. And for our chosen signal resolution `N=10000`, these to vary very little, so we only get a constant signal with some initial offset.

# Task 2: Calculator (Sequence-to-sequence) [7 P] #

## I. Train the provided network to ~100% valuation accuracy by choosing suitable hyperparameters for the RNN and comment on the result. [2 P] ##

## II. Explore the limitations of the network to generalize to different expressions. How could the generalization capability be improved? [5 P] ##

# References #

- [1] RWTH DLiPR SS20 Part 9: Generative Methods. Lecture Slides. 2020.
- [2] Andrej Karpathy. The Unreasonable Effectiveness of Recurrent Neural Networks. May 21, 2015. URL: https://karpathy.github.io/2015/05/21/rnn-effectiveness/
- [3] Christopher Olah. Understanding LSTM Networks. August 27, 2015. URL: https://colah.github.io/posts/2015-08-Understanding-LSTMs/
- [4] Hackernoon. Understanding architecture of LSTM cell from scratch with code. June 18, 2018. URL: https://hackernoon.com/understanding-architecture-of-lstm-cell-from-scratch-with-code-8da40f0b71f4




