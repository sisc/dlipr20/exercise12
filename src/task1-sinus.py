# ==============================================================================
# Constants, Imports
# ==============================================================================
# constants
COMET_EXPERIMENT = True
NOTEBOOK = False
REFERENCES = [
        "https://git.rwth-aachen.de/3pia/vispa/dlipr", # [1]
        "https://github.com/DavidWalz/dlipr", # [2]
    ]


# setup comet
# for notebook (eg google colab). only needed if comet_ml not installed yet.
if COMET_EXPERIMENT and NOTEBOOK:
    pass # if notebook, uncomment:
    # %pip install comet_ml
# after installation for first time, restart notebook!

if COMET_EXPERIMENT:
    # import comet_ml in the top of your file    
    from comet_ml import Experiment
    
    # Add the following code anywhere in your machine learning file
    experiment = Experiment(api_key="EnterYourAPIKey",
                            project_name="bonus", workspace="irratzo")
       
# imports from provided task1-sinus.py
# from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from tensorflow import keras
layers = keras.layers

# # other imports  
# import tensorflow as tf
# from sklearn.preprocessing import StandardScaler
# import os
from pathlib import Path
# models = keras.models
# layers = keras.layers                            

# some utility logging methods    
def printcomet(string : str = ""):
    # append references (like for "... [1] ..." and so on)
    reflabl = ["[{}]".format(i+1) for i in range(len(REFERENCES))]
    if any([labl in string for labl in reflabl]):
      string += "\nReferences:"
      for i,labl in enumerate(reflabl):
        if labl in string:
          string += "\n{} {}".format(labl, REFERENCES[i])
    
    # print and log
    print(string)
    if COMET_EXPERIMENT:
        experiment.log_text(string)
        
def log_image(filename : str = "unnamed", path : Path = None):
    # the nice thing about vispa is that if the image stays
    # in the src folder, it will be displayed upon execution.
    # the bad thing about that is that it dirties the src folder.
    # alternative: put into a data folder.
    if path:
        path.mkdir(parents=True, exist_ok=True)
        Path(filename).replace(path / filename)
    else:
        path = Path.cwd() # = don't move file
    if COMET_EXPERIMENT:
        experiment.log_image(str(path / filename))
        
def plot_and_log_image(filename : str = "unnamed", path : Path = None, array=None, 
                       figsize=(10,10), cmap=plt.cm.Greys, axis='off'):
    fig, ax = plt.subplots(figsize=figsize)
    ax.imshow(array, cmap=cmap)
    ax.axis('off')
    plt.tight_layout()
    fig.savefig(filename)
    log_image(filename, path)     
    
def stats(array):
    printcomet("shape {}, type {}".format(array.shape, array.dtype))
    printcomet("(min,max)=({:.2e},{:.2e}), mean={:.2e}, std={:.2e}".format(
        np.amin(array), np.amax(array), np.mean(array), np.std(array)))
    
# preprocess the data in a suitable way
def standardize(*arrays):
    """first array used for mean, std. creates copies.
    
    Examples
    ========
    
    >>> x_train = standardize(x_train)
    >>> x_train, x_test = standardize(x_train, x_test)
    >>> x_train, x_test, x_foo = standardize(x_train, x_test, x_foo)
    """
    first = arrays[0]
    rest = arrays[1:]
    astype = 'float32'

    shape = first.shape
    M = shape[0] # no. of data points
    N = first.size//M # size of one data point
    firs = first.flatten().reshape(M, N) # scaler needs 1D data
    scaler = StandardScaler()
    scaler.fit(firs)
    firs = scaler.transform(firs).reshape(*shape).astype(astype) # CNN needs 2+D data
    
    arrs = [firs,]
    for arr in rest:
        shape = arr.shape
        M = shape[0]
        N = arr.size//M
        arrs.append(scaler.transform(
            arr.flatten().reshape(M,N)).reshape(*shape).astype(astype))

    # # alternative equivalent method, less precise:
    #
    # mean = np.mean(first, axis=0)
    # stdev = np.std(first, axis=0)
    # firs = np.zeros_like(first).astype(astype)
    # for i in range(first.shape[0]):
    #   firs[i] = (first[i] - mean) / stdev
    # arrs = [firs,]
    # for arr in rest:
    #     ar = np.zeros_like(arr).astype(astype)
    #     for i in range(arr.shape[0]):
    #       ar[i] = (arr[i] - mean) / stdev
    #     arrs.append(ar)
    
    if len(arrs)==1:
        return arrs[0]
    else:
        return tuple(arrs)
        
# make output directory ../data/results_taskX.Y_runZ
output_label = "task1.III_ws-05_run1"
path_src = Path.cwd()
path_data = path_src.parent / "data" / output_label
path_models = path_src.parent / "models" / output_label
printcomet("local output dirs:\n- data: {}\n- models: {}\n- src: {}".format(path_data, path_models, path_src))

printcomet("Constants: COMET_EXPERIMENT {}, NOTEBOOK {}".format(
    COMET_EXPERIMENT, NOTEBOOK))        
        
# -----------------------------------------------
# Data
# -----------------------------------------------
# create a signal trace: t = 0-100, f = sin(pi * t)
N = 10000
t = np.linspace(0, 100, N)  # time steps
f = np.sin(np.pi * t)  # signal

# split into semi-redundant sub-sequences of length = window_size + 1
window_size = 5
n = N - window_size - 1  # number of possible splits # N=1e4, ws=20: n=9979.
data_unstacked = [f[i: i + window_size + 1] for i in range(n)]
# data_unstacked = list of size n, elements of size ws+1:
# for N=1e4, ws=20: 
#   list of size 9979, elements of size 21: 
#   contain redundant data points of signal f, indices in t / f:
#   [0-20, 1-21, 2-22, ..., 9978-9998, n=9979-9999]
# for N=1e4, ws=1:
#   list of size 9998, elements of size  1:
#   contain redundant data points of signal f, indices in t / f:
#   [0-1, 1-2, 2-3, ..., 9997-9998, 9998-9999]
data = np.stack(data_unstacked)
# same as list, so shape = (n, ws+1)
# for N=1e4, ws=20: shape = (9979, 21)
# for N=1e4, ws= 1: shape = (9998,  2)

# shuffle the order of the sub-sequences
np.random.shuffle(data)

# split into features (first <window_size> steps) and targets (last step)
X, y = np.split(data, [-1], axis=1)
X = X[..., np.newaxis]

print('Example:')
print('X =', X[0, :, 0])
print('y =', y[0, :])
# for N=1e4, ws=20: 
#   data unshuffled contains redundant data points of signal f, indices:
#   [0-20, 1-21, 2-22, ..., 9978-9998, n=9979-9999]
#   unshuffled, split would lead to, e.g. (again, indices in f):
#           X            y
#   [   0, ...,   19] [  20]
#   [   1, ...,   20] [  21]
#   ...
#   [9978, ..., 9997] [9998]
#   [9979, ..., 9998] [9999]
# for N=1e4, ws=1:
#   data unshuffled contains redundant data points of signal f, indices:
#   [0-1, 1-2, 2-3, ..., 9997-9998, 9998-9999]
#   unshuffled, split would lead to, e.g. (again, indices in f):
#      X     y
#   [   0] [  1]
#   [   1] [  2]
#   ...
#   [9997] [9998]
#   [9998] [9999]


# -----------------------------------------------
# Model & Training
# -----------------------------------------------
units_per_LSTM_layer = [16]
z0 = layers.Input(shape=[None, 1])
LSTM_layers = []
for i, units in enumerate(units_per_LSTM_layer):
    if i==0:
        LSTM_layers.append( layers.LSTM(units)(z0) )
    else:
        LSTM_layers.append( layers.LSTM(units)(LSTM_layers[-1]) )
z = layers.Dense(1)(LSTM_layers[-1])
model = keras.models.Model(inputs=z0, outputs=z)
print(model.summary())

model.compile(loss='mse', optimizer='adam')

# print info on parameters varied in task1 (also used for plt.title)
param_info =  "LSTM={}, ws={}".format(units_per_LSTM_layer, window_size)
printcomet("Parameter info: " + param_info)


def train():
    model.fit(X, y,
        epochs=60,
        batch_size=32,
        verbose=2,
        validation_split=0.1,
        callbacks=[
            keras.callbacks.ReduceLROnPlateau(factor=0.67, patience=3, verbose=1, min_lr=1E-5),
            keras.callbacks.EarlyStopping(patience=4, verbose=1)])

# docstring for keras.callbacks.ReduceLROnPlateau:
# Reduce learning rate when a metric has stopped improving.
# Models often benefit from reducing the learning rate by a factor of 2-10 once
# learning stagnates. This callback monitors a quantity and if no improvement is
# seen for a 'patience' number of epochs, the learning rate is reduced.
# defaults:
# monitor="val_loss"
    
if COMET_EXPERIMENT:
    # 'with'-context logs metrics with the prefix 'train_'
    with experiment.train():
        train()
else:
    train()


# -----------------------------------------------
# Evaluation
# -----------------------------------------------
# note: gonna use visual evaluation via prediction plotting againgst data,
# so no need for 'wich experiment.test():' context.

def predict_next_k(model, window, k=10):
    """Predict next k steps for the given model and starting sequence """
    x = window[np.newaxis, :, np.newaxis]  # initial input
    y = np.zeros(k)
    for i in range(k):
        y[i] = model.predict(x, verbose=0)
        # create the new input including the last prediction
        x = np.roll(x, -1, axis=1)  # shift all inputs 1 step to the left
        x[:, -1] = y[i]  # add latest prediction to end
    return y


def plot_prediction(i0=0, k=500, fname=None):
    """ Predict and plot the next k steps for an input starting at i0 """
    y0 = f[i0: i0 + window_size]  # starting window (input)
    y1 = predict_next_k(model, y0, k)  # predict next k steps

    t0 = t[i0: i0 + window_size]
    t1 = t[i0 + window_size: i0 + window_size + k]

    plt.figure(figsize=(12, 4))
    plt.plot(t, f, label='data')
    plt.plot(t0, y0, color='C1', lw=3, label='prediction')
    plt.plot(t1, y1, color='C1', ls='--')
    plt.xlim(0, 10)
    plt.legend()
    plt.xlabel('$t$')
    plt.ylabel('$f(t)$')
    plt.title("task1, " + param_info + ", i0={}, k={}".format(i0,k))
    if fname is not None:
        plt.savefig(fname, bbox_inches='tight')


filename = 'sinus-1.png'
plot_prediction(12, fname=filename)
log_image(filename=filename, path=path_data)

filename = 'sinus-2.png'
plot_prediction(85, fname=filename)
log_image(filename=filename, path=path_data)

filename = 'sinus-3.png'
plot_prediction(115, fname=filename)
log_image(filename=filename, path=path_data)
