from comet_ml import Experiment
import numpy as np
import dlipr
from tensorflow import keras
layers = keras.layers


# Set up YOUR experiment - login to comet, create new project (for new exercise)
# and copy the statet command
# or just change the name of the workspace, and the API (you can find it in the settings)
experiment = Experiment(api_key="EnterYourAPIKey",
                        project_name="Bonus", workspace="EnterGroupWorkspaceHere")


# characters to consider
alphabet = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '-', ' ']
n_chars = len(alphabet)


def onehot_encode(string):
    """ Convert string to sequence of one-hot encoded characters """
    V = np.eye(len(alphabet))
    encode = dict((c, V[i]) for i, c in enumerate(alphabet))
    return np.array([encode[c] for c in string])


def onehot_decode(sequence):
    """ Convert sequence of one-hot encoded characters to string """
    if len(sequence) == 1:
        sequence = sequence[0]
    decode = dict((i, c) for i, c in enumerate(alphabet))
    return ''.join([decode[np.argmax(v)] for v in sequence])


def make_data(N, nums=2, low=-99, high=99):
    """ Generate dataset (X, y) of sequences of one-hot encoded characters
    X (array, shape=[N, sx, n_chars]): sum of nums random numbers from low to high
    y (array, shape=[N, sy, n_chars]): result
    """
    X = np.random.randint(low=low, high=high + 1, size=(N, nums))
    y = np.sum(X, axis=1)

    # form string of each expression to strings
    X = ['+'.join(it) for it in X.astype(str)]
    y = y.astype(str)

    # calculate maximum size of strings for X and y
    sx = int(nums * np.ceil(np.log10(max(-low, high)) + 1))
    sy = int(np.ceil(np.log10(nums * max(-low, high))) + 1)

    # clean up strings ("+-" --> "-") and left-pad with spaces
    X = [it.replace('+-', '-').rjust(sx) for it in X]
    y = [it.rjust(sy) for it in y]

    # one-hot encode each character
    X = np.array([onehot_encode(i) for i in X])
    y = np.array([onehot_encode(i) for i in y])
    return X, y


# -----------------------------------------------
# Data
# -----------------------------------------------
X, y = make_data(10000, 2)
sy = len(y[0])  # length of the answer string

print('Example:')
a = onehot_decode(X[500])
b = onehot_decode(y[500])
print('%s = %s\n' % (a, b))


# -----------------------------------------------
# Model & Training
# -----------------------------------------------
z0 = layers.Input(shape=((None, n_chars)))
z = layers.LSTM(16)(z0)
z = layers.RepeatVector(sy)(z)
z = layers.LSTM(16, return_sequences=True)(z)
z = layers.TimeDistributed(layers.Dense(n_chars, activation='softmax'))(z)
model = keras.models.Model(inputs=z0, outputs=z)
print(model.summary())

model.compile(
    loss='categorical_crossentropy',
    optimizer='adam',
    metrics=['accuracy'])

model.fit(X, y,
    epochs=100,
    batch_size=16,
    validation_split=0.05,
    verbose=2,
    callbacks=[
               keras.callbacks.ReduceLROnPlateau(factor=0.67, patience=3, verbose=1),
               keras.callbacks.EarlyStopping(patience=5, verbose=1)])


# -----------------------------------------------
# Evaluation
# -----------------------------------------------
# try 10 new operations
print('\nTest examples (correct answers in brackets, just in case)')
N = 10
X, y = make_data(N, 2)
yp = model.predict(X, verbose=0)

for i in range(N):
    a = onehot_decode(X[i])
    b = onehot_decode(yp[i])
    c = onehot_decode(y[i])
    print('%s = %s (%s)' % (a, b, c))


# try own expressions
print('\nMore test examples')
def test(a):
    x = onehot_encode(a)[np.newaxis]
    yp = model.predict(x)
    b = onehot_decode(yp)
    print('%s = %s' % (a, b))

test('48-11')
test(' 48-11')
test('-11+48')
test('-11 +48')
test(' -11 +48')
